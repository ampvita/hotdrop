package com.gethotdrop.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import com.gethotdrop.android.data.Storage;
import com.gethotdrop.android.views.drop.DropActivity;
import com.gethotdrop.android.views.feed.FeedActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

public class SyncService extends Service {
    /*
    Hotdrop Configuration Settings
    (all distance in meters)
     */
    public static final float VIEW_ACCURACY = 500;
    public static final float POST_ACCURACY = 400;
    public static final double DROP_RADIUS = 400;

    private static final int REFRESH_INTERVAL = 300;
    public static final boolean MOCK_ENABLED = false;


    public static boolean gpsStatus;
    public static String GcmId;

    private static boolean isForeground;

    public static LocationClient lClient;
    protected static Context context;
    private android.os.Handler mHandler;
    private Storage dStore;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        LocationHandler listen = LocationHandler.getListener(this);
        lClient = new LocationClient(this, listen, listen);
        lClient.connect();

        // Use high accuracy

        dStore = Storage.initialize(this);
        mHandler = new android.os.Handler();
        this.startRepeatingTask();
        return Service.START_STICKY;
    }



    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            Intent i = new Intent(context, Worker.class);

            if (GcmId == null) {
                GcmId = PushReceiver.getGcmId(context);
            }

            if (dStore.getCurrentDrop() != 0) {
                i.putExtra("action", 2);
                i.putExtra("dropId", dStore.getCurrentDrop());
                context.startService(i);
                DropActivity.updateComments();
            }

            //Log.d("Runnable", "State:" + state);
            mHandler.postDelayed(mStatusChecker, REFRESH_INTERVAL);
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    public static void isForeground(boolean foreground) {
        isForeground = foreground;
        if (lClient != null ) {
            LocationHandler lHandler = LocationHandler.getListener(context);
            LocationRequest lRequest = LocationRequest.create();
            lClient.removeLocationUpdates(lHandler);
            if (foreground) {
                lRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                lRequest.setInterval(5000);
                lRequest.setFastestInterval(1000);
            } else {
                lRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                lRequest.setInterval(30000);
                lRequest.setFastestInterval(6000);
            }
            lClient.requestLocationUpdates(lRequest, lHandler);
        }
    }

    public static boolean isForeground() {
        return isForeground;
    }



    @Override
    public void onDestroy() {
        lClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }




}
