package com.gethotdrop.android.services;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.gethotdrop.android.data.Storage;
import com.gethotdrop.android.views.feed.FeedActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class LocationHandler implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener,
		LocationClient.OnRemoveGeofencesResultListener,
		LocationClient.OnAddGeofencesResultListener, LocationListener {

	private static LocationHandler listen;

	private Context context;



	private LocationHandler(Context c) {
		context = c;
	}

	public static LocationHandler getListener(Context c) {
		if (listen == null) {
			listen = new LocationHandler(c);
		}
		return listen;
	}

	@Override
	public void onRemoveGeofencesByPendingIntentResult(int arg0,
			PendingIntent arg1) {}

	@Override
	public void onRemoveGeofencesByRequestIdsResult(int arg0, String[] arg1) {}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {}

	@Override
	public void onConnected(Bundle arg0) {
		if (!SyncService.MOCK_ENABLED) {
			LocationRequest lRequest;
			lRequest = LocationRequest.create();
			lRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			// TODO: investigate what interval is (perhaps set lower)
			lRequest.setInterval(5000);
			lRequest.setFastestInterval(1000);
			SyncService.lClient.requestLocationUpdates(lRequest, this);
			Log.w("LocationListener", "Connected, Request made");
            Intent i = new Intent(context, Worker.class);
            i.putExtra("action", 0); // updating lists of drops
            context.startService(i);

		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAddGeofencesResult(int arg0, String[] arg1) {
		Log.e("geofence", "result");
	}

	@Override
	public void onLocationChanged(Location location) {
        if (location != null && location.getAccuracy() <= SyncService.VIEW_ACCURACY) {
            Storage s = Storage.getInstance();
            if (s != null) {
                Location last = s.getLastLocation();
                if (last==null || last.distanceTo(location) > 50) {
                    Intent i = new Intent(context, Worker.class);
                    i.putExtra("action", 0); // updating lists of drops
                    context.startService(i);
                }
                s.updateActiveDropsList(location);
                FeedActivity.gpsHealthy(true);
                if (s.newDrop && !SyncService.isForeground()) {
                    Intent i = new Intent(context, Worker.class);
                    i.putExtra("action", 4);
                    context.startService(i);
                }
            }
        } else {
            FeedActivity.gpsHealthy(false);
        }
        FeedActivity.updateDrops();

	}

}
