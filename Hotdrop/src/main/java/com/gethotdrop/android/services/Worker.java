package com.gethotdrop.android.services;

import com.gethotdrop.android.R;
import com.gethotdrop.android.data.Comment;
import com.gethotdrop.android.data.Storage;
import com.gethotdrop.android.data.Server;
import com.gethotdrop.android.views.feed.FeedActivity;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Vibrator;
import android.util.Log;

import java.util.ArrayList;

public class Worker extends IntentService {


    public Worker() {
        super("UpdateHotdropService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        switch (intent.getIntExtra("action", 0)) {

            case 0: //update location model
                refreshDropStore();
                break;
            case 1: //post drop
                String message = intent.getStringExtra("message");
                String media = intent.getStringExtra("media");
                postDrop(message, media);
                break;
            case 2: // update comments
                refreshCommentStore(intent);
                break;
            case 3:
                String comment = intent.getStringExtra("message");
                int dropId = intent.getIntExtra("dropId", 0);
                postComment(dropId, comment);
                break;
            case 4: // create notification
                createNotification();
                break;
        }
    }

    private void refreshDropStore() {
        if (SyncService.lClient.isConnected()) {
            Location loc = SyncService.lClient.getLastLocation();
            if (loc != null) {
                if (loc.getAccuracy() <= SyncService.VIEW_ACCURACY) {
                    Log.v("Worker: refreshDropStore", "Loc: " + loc.getLatitude()
                            + ", " + loc.getLongitude());

                    Storage ds = Storage.getInstance();
                    if (ds != null) {
                        ds.refreshDrops(loc);

                    } else
                        Log.e("Worker: refreshDropStore", "Drop Store Failed");
                } else
                    Log.w("Worker: refreshDropStore",
                            "Accuracy too low: " + loc.getAccuracy());
            } else
                Log.e("Worker: refreshDropStore", "Location was null");
        } else
            Log.e("Worker: refreshDropStore", "Provider not connected");
    }

    private void postDrop(String message, String media) {
        if (SyncService.lClient.isConnected()) {
            Location loc = SyncService.lClient.getLastLocation();
            if (loc != null) {
                if (loc.getAccuracy() <= SyncService.POST_ACCURACY) {
                    Log.i("Worker: postDrop", "Loc: " + loc.getLatitude()
                            + ", " + loc.getLongitude());
                    if (!Server.createDrop(loc.getLatitude(), loc.getLongitude(), message, media)) {
                        Log.e("Worker", "Could not create drop. Likely a networking error.");
                    }
                } else
                    Log.e("Worker: postDrop",
                            "Accuracy too low: " + loc.getAccuracy());

            } else
                Log.e("Worker: postDrop", "Location was null");
        } else
            Log.e("Worker: postDrop", "Provider not connected");
    }

    private void refreshCommentStore(Intent i) {
        int dropId = i.getIntExtra("dropId", 0);
        if (dropId != 0) {
            ArrayList<Comment> updatedComments = Server.getComments(dropId);
            if (updatedComments != null && Storage.getCurrentDrop() == dropId) {
                Storage.getInstance().updateCommentStore(dropId, updatedComments);
            }

        }
    }


    public static boolean canPost() {
        if (SyncService.lClient.isConnected()) {
            Location loc = SyncService.lClient.getLastLocation();
            if (loc != null && loc.getAccuracy() <= SyncService.POST_ACCURACY) {
                return true;
            }
        }
        return false;
    }

    public void postComment(int dropId, String message) {
        Server.createComment(dropId, message);
    }

    public void createNotification() {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(this, com.gethotdrop.android.views.feed.FeedActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Build notification
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Discovered Hotdrop")
                .setContentText("You have a new drop!").setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent).getNotification();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        //Launch notification
        notificationManager.notify(0, noti);

        //Get Vibrator service
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Vibrate for 300 milliseconds
        v.vibrate(300);
    }

}