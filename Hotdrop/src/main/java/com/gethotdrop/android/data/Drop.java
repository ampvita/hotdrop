package com.gethotdrop.android.data;

import java.security.InvalidParameterException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.android.gms.location.Geofence;

import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Drop {
	private int id;
	private double latitude;
	private double longitude;
	public String message;
	private String createdAt;
	private Date updatedAt;
	private int grabs;
    private String media;

	public Drop(int id, double latitude, double longitude, // int userId
			String message, String createdAt, Date updatedAt, int grabs, String media) {
		this.id = id;
		//this.userId = userId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.message = message;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.grabs = grabs;
        this.media = media;
	}

    static public Drop fromJson(JSONObject json) {
        try {
            String objType = json.getString("objType");
            if (!objType.equals("drop")) return null;

            // get drop data
            int id = json.getInt("id");
            String message = json.getString("message");
            Double latitude = json.getDouble("lat");
            Double longitude = json.getDouble("lng");
            String media = json.getString("media");

            String dateString = json.getString("created");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ", Locale.US);
            Date created = sdf.parse(dateString, new ParsePosition(0));

            Drop drop = new Drop(id, latitude, longitude, message, dateString, created, 0, media);
            return drop;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


	public int getId() {
		return id;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getMessage() {
		return message;
	}

	public String getCreatedAt() {
		return createdAt.toString();
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public Location getLocation() {
		Location location = new Location(LocationManager.PASSIVE_PROVIDER);
		location.setLatitude(latitude);
		location.setLongitude(longitude);
		return location;

	}
    @Override
    public String toString(){
        return "ID: " + id + ", Message: " + message + ", Lat: " + latitude + ", Lng: " + longitude
                + ", Date: " + createdAt.toString();
    }
	public Geofence toGeofense() {
        	// Build a new Geofence object
            return new Geofence.Builder()
            	.setRequestId(String.valueOf(getId()))
            	.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
            	.setCircularRegion(getLatitude(), getLongitude(), (float) 40)
            	.setExpirationDuration(Geofence.NEVER_EXPIRE)
            	.build();
            	
        }

    public JSONObject toJSON() {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("objType", "drop");
            jObj.put("id", id);
            jObj.put("lat", latitude);
            jObj.put("lng", longitude);
            jObj.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObj;
    }

	public int getGrabs() {
		return grabs;
	}

	public void setGrabs(int grabs) {
		this.grabs = grabs;
	}
}