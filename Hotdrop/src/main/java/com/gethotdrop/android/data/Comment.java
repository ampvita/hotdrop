package com.gethotdrop.android.data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Dan on 12/2/13.
 */
public class Comment {
    private String message;
    private String created;

    public Comment(String messageIn, String createdIn) {
        message = messageIn;
        created = createdIn;
    }

    public String getMessage(){
        return message;
    }
    public String getCreated() {
        return created;
    }

    public static ArrayList<Comment> fromJsonArray (JSONArray obj) {
        ArrayList<Comment> commentList = new ArrayList<Comment>();
        if (obj == null) {
            Log.e("Comment", "Could not download comments. Likely a network error.");
            return null;
        }
        try {
            for (int i = 0; i <obj.length(); i++) {
                JSONObject j = obj.getJSONObject(i);
                String message = j.getString("message");
                String created = j.getString("created");
                Comment c = new Comment(message, created);
                commentList.add(c);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return commentList;
    }
}
