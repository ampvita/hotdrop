package com.gethotdrop.android.data;

import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dan on 12/2/13.
 */
public class Server {
    private static final String TAG = "Server";

    //  Public Server URL for all HTTP Requests
    final static private String SERVER_URL = "http://coldgrab.com:8000";

    public static boolean createComment(int dropId, String message) {
        String url = SERVER_URL + "/api/comments/create/";

        String uuid = Storage.getUuid();
        JSONObject params = new JSONObject();
        try {
            params.put("drop", dropId);
            params.put("message", message);
            params.put("uuid", uuid);
            Log.i(TAG, params.toString(2));
            JSONArray result = HttpPostRequest(url, params);
            for (int i = 0; i < result.length(); i++) {
                JSONObject j = result.getJSONObject(i);
                String messageCheck = j.getString("message");
                if (!messageCheck.equals(message)) {
                    Log.e(TAG + ":createComment", j.toString());
                    return false;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;


    }

    public static ArrayList<Comment> getComments(int dropId) {
        String url = SERVER_URL + "/api/comments/";
        JSONObject params = new JSONObject();
        try {
            params.put("drop", dropId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray result = HttpPostRequest(url, params);
        return Comment.fromJsonArray(result);
    }

    public static boolean createDrop(double latitude, double longitude, String message, String media) {
        String url = SERVER_URL + "/api/drops/create/";

        String uuid = Storage.getUuid();
        if (media == null) {
            media = " ";
        }

        JSONObject params = new JSONObject();
        try {
            params.put("lat", latitude);
            params.put("lng", longitude);
            params.put("message", message);
            params.put("uuid", uuid);
            params.put("media", media);
            Log.i(TAG, params.toString());

            JSONArray result = HttpPostRequest(url, params);
            if (result == null) return false;
            for (int i = 0; i < result.length(); i++) {
                JSONObject j = result.getJSONObject(i);
                String messageCheck = j.getString("message");
                if (!messageCheck.equals(message)) {
                    Log.e(TAG + ":createDrop", j.toString());
                    return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;

    }

    public static Map<Integer, Drop> heartbeat(double latitude, double longitude) {
        String url = SERVER_URL + "/api/hb/";
        JSONObject params = new JSONObject();
        String uuid = Storage.getUuid();
        Map<Integer, Drop> drops = new HashMap<Integer, Drop>();
        try {
            params.put("lng", longitude);
            params.put("lat", latitude);
            params.put("uuid", uuid);
            JSONArray result = HttpPostRequest(url, params);
            if (result != null) {
                for (int i = 0; i < result.length(); i++) {
                    JSONObject obj = result.getJSONObject(i);
                    obj.put("objType", "drop");
                    Drop d = Drop.fromJson(obj);
                    drops.put(d.getId(), d);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return drops;
    }

    @Deprecated
    public static Map<Integer, Drop> getDrops(double latitude, double longitude) {
        String url = SERVER_URL + "/api/drops/";
        JSONObject params = new JSONObject();
        Map<Integer, Drop> drops = new HashMap<Integer, Drop>();
        try {
            params.put("lng", longitude);
            params.put("lat", latitude);
            JSONArray result = HttpPostRequest(url, params);
            if (result != null) {
                for (int i = 0; i < result.length(); i++) {
                    JSONObject obj = result.getJSONObject(i);
                    obj.put("objType", "drop");
                    Drop d = Drop.fromJson(obj);
                    drops.put(d.getId(), d);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return drops;
    }



    public static boolean registerGcmId(String reg, String uuid) {
        String url = SERVER_URL + "/gcm/v1/device/register/";
        JSONObject params = new JSONObject();
        try {
            params.put("dev_id", uuid);
            params.put("reg_id", reg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpPostRequest(url, params);
        return true;
    }

    private static JSONArray HttpPostRequest(String url, JSONObject outgoingObj) {
        try {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url);

            StringEntity se = new StringEntity(outgoingObj.toString());

            // setup HTTP parameters
            postRequest.setEntity(se);
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Content-type", "application/json");

            long time = System.currentTimeMillis();
            HttpResponse response = (HttpResponse) client.execute(postRequest);
            Log.d(TAG, "HTTPResponse received in [" + (System.currentTimeMillis() - time) + "ms]");

            // get response data
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                // read stream
                InputStream in = entity.getContent();
                Header contentEncoding = response.getFirstHeader("Content-Encoding");

                String resultString = convertStreamToString(in);
                in.close();
                JSONArray incomingObjs = new JSONArray();
                if (resultString.charAt(0) == '[') {
                    incomingObjs = new JSONArray(resultString);
                } else {
                    JSONObject obj = new JSONObject(resultString);
                    incomingObjs.put(obj);
                }
                return incomingObjs;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         *
         * (c) public domain: http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/11/a-simple-restful-client-at-android/
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}

