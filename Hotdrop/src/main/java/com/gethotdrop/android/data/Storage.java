package com.gethotdrop.android.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.gethotdrop.android.services.SyncService;

/* 
 * Two types of drop lists, active and aware of
 * Active - needs to provide a list to DropArrayAdapter
 * 		  - maintain list of current drops
 * 
 * Passive - needs to coordinate with geofence
 *
 */

public class Storage {
    // storage
    private Location lastLocation;
    private static Storage instance = null;
    private String uuid;

    protected Storage(Context context) {
        uuid = Unique.id(context);
    }

    public static Storage initialize(Context context) {
        if (instance == null)
            instance = new Storage(context);
        return instance;
    }


    public static Storage getInstance() {
        return instance;
    }

    public static String getUuid() {
        return instance.uuid;
    }

    public List<Drop> getDropList() {
        if (drops == null)
            drops = new ArrayList<Drop>();
        return drops;
    }

    // drops
    private ArrayList<Drop> drops;
    private Map<Integer, Drop> activeDrops = new HashMap<Integer, Drop>();
    private Map<Integer, Drop> allDrops = new HashMap<Integer, Drop>();
    private Map<Integer, Drop> newActiveDrops = new HashMap<Integer, Drop>();
    public boolean refreshDrops(Location location) {
        if (location == null) {
            return false;
        }
        Map<Integer, Drop> newAllDrops;
        try {
            newAllDrops = Server.heartbeat(location.getLatitude(),
                    location.getLongitude());
        } catch (Exception e) {
            return false;
        }
        lastLocation = location;
        allDrops = newAllDrops;
        return true;
    }

    public boolean refreshActiveDrops(Location location) {
        double radius = SyncService.DROP_RADIUS;
        newActiveDrops = new HashMap<Integer, Drop>();
        for (Drop drop : allDrops.values()) {
            if (drop.getLocation().distanceTo(location) <= radius) {
                newActiveDrops.put(drop.getId(), drop);
            }
        }

        if (equalMaps(activeDrops, newActiveDrops))
            return false; // false if they didn't change
        else {
            newDrop = isNewDrop();
            activeDrops = newActiveDrops;
            return true;
        }
    }

    public boolean newDrop = false;
    public boolean isNewDrop() {
        int i = 0;
        for (Integer k : newActiveDrops.keySet()) {
            if (activeDrops.containsKey(k))
                i += 1;
        }
        activeDrops = newActiveDrops;
        if (i != newActiveDrops.keySet().size())
            return true;
        return false;
    }

    public boolean equalMaps(Map<Integer, Drop> a, Map<Integer, Drop> b) {
        if (a.size() != b.size())
            return false;
        for (Integer k : a.keySet()) {
            if (!(b.containsKey(k)))
                return false;
        }
        return true;
    }

    public Map<Integer, Drop> getActiveDrops() {
        return activeDrops;
    }

    public void updateActiveDropsList(Location location) {
        this.refreshActiveDrops(location);
        ArrayList<Drop> dropList = new ArrayList<Drop>();

        for (Drop drop : activeDrops.values()) {
            dropList.add(drop);
        }
        Collections.sort(dropList, new Comparator<Drop>() {

            @Override
            public int compare(Drop arg0, Drop arg1) {
                return -arg0.getCreatedAt().compareTo(arg1.getCreatedAt());
            }

        });
        drops.clear();
        drops.addAll(dropList);
    }

    public Map<Integer, Drop> getAllDrops() {
        return allDrops;
    }

    // comments
    private ArrayList<Comment> comments;
    private Map<Integer, ArrayList<Comment>> commentStore = new HashMap<Integer, ArrayList<Comment>>();
    private int currentDrop = 0;

    public static int getCurrentDrop() {
        return instance.currentDrop;
    }

    public static void clearCurrentDrop() { if (instance!=null) instance.currentDrop = 0;}

    public List<Comment> getCommentList(int dropId) {
        currentDrop = dropId;
        if (comments == null || !commentStore.containsKey(dropId)) {
            comments = new ArrayList<Comment>();
            comments.add(new Comment("loading...", null));
        }
        this.refreshCommentsList(dropId);
        return comments;
    }

    public void refreshCommentsList(int dropId) {
        currentDrop = dropId;
        ArrayList<Comment> commentList = commentStore.get(currentDrop);
        if (commentList != null) {
            Collections.sort(commentList, new Comparator<Comment>() {
                @Override
                public int compare(Comment arg0, Comment arg1) {
                    return -arg0.getCreated().compareTo(arg1.getCreated());
                }
            });
            comments.clear();
            comments.addAll(commentList);
        }
    }

    public void updateCommentStore(int i, ArrayList<Comment> c) {
        // check if there are any comments
        if (c.size() == 0) {
            c.add(new Comment("No comments.", null));
        }
        commentStore.put(i, c);
        refreshCommentsList(i);
    }

    public Location getLastLocation() {
        return lastLocation;

    }

}
