package com.gethotdrop.android.views.feed;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.gethotdrop.android.data.Drop;
import com.gethotdrop.android.services.Worker;

import java.util.Calendar;
import java.util.Date;


public class ButtonListeners {
    public static class PostButton implements View.OnClickListener {
        private FeedActivity a;

        public PostButton(FeedActivity feed) {
            a = feed;
        }

        @Override
        public void onClick(View v) {
            String noteText = a.postNote.getText().toString();
            if (a.chosenImage == null && (noteText.equals(""))) {
                return;
            }

            String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
            if (Worker.canPost()) {
            Intent postIntent = new Intent(a, Worker.class);
            postIntent.putExtra("action", 1);
            postIntent.putExtra("message", noteText);
            a.startService(postIntent);

           //a.drops.add(newDrop);
            //a.adapter.notifyDataSetChanged();

            a.postNote.setText("");
            a.chosenImage = null;
            a.postImage.setVisibility(View.GONE);
            } else {
                a.sendToast("Accuracy too low to post.", Toast.LENGTH_SHORT);
            }
        }
    }

    public static class UploadButton implements View.OnClickListener {
        private FeedActivity a;

        public UploadButton(FeedActivity feed) {
            a = feed;
        }

        @Override
        public void onClick(View v) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            a.startActivityForResult(photoPickerIntent, 1);
        }
    }

    public static class CameraButton implements View.OnClickListener {
        private FeedActivity a;

        public CameraButton(FeedActivity feed) {
            a = feed;
        }

        @Override
        public void onClick(View v) {
            Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
            a.startActivityForResult(cameraIntent, 0);

            //Code snippet for better quality images
//				ContentValues values = new ContentValues();
//		            values.put(MediaStore.Images.Media.TITLE, "New Picture");
//		            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
//		            imageUri = getContentResolver().insert(
//		                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//		            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//		            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//		            startActivityForResult(intent, 0);
        }
    }
}
