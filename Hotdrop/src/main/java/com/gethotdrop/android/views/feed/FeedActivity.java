package com.gethotdrop.android.views.feed;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.newrelic.agent.android.NewRelic;

import java.util.List;

import com.gethotdrop.android.R;
import com.gethotdrop.android.data.Storage;
import com.gethotdrop.android.services.SyncService;
//import com.gethotdrop.android.service.SyncService;
import com.gethotdrop.android.data.*;

import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class FeedActivity extends Activity {
    public static DropArrayAdapter adapter;
    protected ListView list;
    public static List<Drop> drops;

    public static AnimationDrawable frameAnimation;
    public static ViewFlipper vf;

    String noteText = null;
    FitToWidthView postImage = null;
    protected Bitmap chosenImage = null;
    protected EditText postNote = null;
    protected String imagePath;
    Uri imageUri;
    protected InputMethodManager imm;
    protected RelativeLayout postButtons;
    private Toast toast;
    private static Context context;


    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        context = this;

        Crashlytics.start(this);
        NewRelic.withApplicationToken(
                "AA173659bd7b9546bb7aadab21d57f8612f18bd7a7"
        ).start(this.getApplication());

        if (checkPlayServices()) {
            // setup flip and gps wait animation
            vf = (ViewFlipper) findViewById(R.id.viewFlipper);
            ImageView gpsAnimation = (ImageView) findViewById(R.id.gpsWait);
            gpsAnimation.setBackgroundResource(R.drawable.gps_wait_animation);
            frameAnimation = (AnimationDrawable) gpsAnimation.getBackground();
            SyncService.gpsStatus = true;
            gpsHealthy(false);

            //Get drops and start service
            startService(new Intent(this, SyncService.class));
            Storage dStore = Storage.initialize(this);
            drops = dStore.getDropList();

            //Create DropArrayAdapter / ListView
            adapter = new DropArrayAdapter(this, R.layout.list_card, drops);
            list = (ListView) findViewById(R.id.list);

            //Create view for image / edittext for note
            postImage = (FitToWidthView) findViewById(R.id.postImage);
            postNote = (EditText) findViewById(R.id.postNote);

            //Set-up listener for what is selected
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            //Create layout that wraps buttons buttons
            postButtons = (RelativeLayout) findViewById(R.id.postButtons);

            //LISTENERS
            final Button postButton = (Button) findViewById(R.id.postButton);
            final ImageButton uploadButton = (ImageButton) findViewById(R.id.uploadButton);
            final ImageButton cameraButton = (ImageButton) findViewById(R.id.cameraButton);

            postButton.setOnClickListener(new ButtonListeners.PostButton(this));
            uploadButton.setOnClickListener(new ButtonListeners.UploadButton(this));
            cameraButton.setOnClickListener(new ButtonListeners.CameraButton(this));

            //LIST CONFIGURATION
            list.setOnItemClickListener(new ListListeners.OnClick(this));
            list.setOverScrollMode(ListView.OVER_SCROLL_ALWAYS);

            list.setOnScrollListener(new ListListeners.OnScroll(this));
            list.setAdapter(adapter); // TODO: ADAPTER SET AT END OF LISTENER
        }
    }

    public static void updateDrops() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    public static void gpsHealthy(boolean okay) {

        if (vf != null && SyncService.gpsStatus != okay) {
            Log.e("cassie", "STOP!");
            if (okay) {
                vf.showPrevious();
                frameAnimation.stop();
                SyncService.gpsStatus = okay;
            } else {
                vf.showNext();
                frameAnimation.start();
                SyncService.gpsStatus = okay;
            }
        }
    }

    //Handles results for camera and image upload intents
    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent buttonIntent) {
        super.onActivityResult(requestCode, resultCode, buttonIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == Activity.RESULT_OK) {
                    chosenImage = (Bitmap) buttonIntent.getExtras().get("data");

//				try {
//	                        Bitmap thumbnail = MediaStore.Images.Media.getBitmap(
//	                                getContentResolver(), imageUri);
//	        				chosenImage = (Bitmap) thumbnail;
//	                      } catch (Exception e) {
//	                        e.printStackTrace();
//	                    }
//				        imm.hideSoftInputFromWindow(list.getWindowToken(), 0);
                }
                break;

            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = buttonIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    BitmapFactory.Options optsDownSample = new BitmapFactory.Options();
                    optsDownSample.inSampleSize = 8;

                    imagePath = filePath;
                    chosenImage = BitmapFactory.decodeFile(filePath, optsDownSample);
                }
                break;
        }
        postImage.setImageBitmap(chosenImage);
        postImage.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.feed, menu);
        return true;
    }

    @Override
     protected void onResume() {
        super.onResume();
        checkPlayServices();
        SyncService.isForeground(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SyncService.isForeground(false);

    }


    public void sendToast(String message, int duration) {
        toast = Toast.makeText(this, message, duration);
        toast.show();
    }

/* images:
    separate thread
    connect to amazon bucket
    aws library client side

    AmazonS3Client s3Client = new AmazonS3Client( new BasicAWSCredentials( MY_ACCESS_KEY_ID, MY_SECRET_KEY ) );
    s3Client.createBucket( MY_PICTURE_BUCKET );
    PutObjectRequest por = new PutObjectRequest( Constants.getPictureBucket(), Constants.PICTURE_NAME, new java.io.File( filePath) );
    s3Client.putObject( por );

*/

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("FeedActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}

//Make buttons blue on click
//postNote.setOnClickListener(new OnClickListener() {
//@Override
//public void onClick(View v) {
//	postButtons.setBackgroundResource(R.color.blue);
//}
//});
