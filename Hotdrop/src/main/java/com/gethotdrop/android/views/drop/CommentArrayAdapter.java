package com.gethotdrop.android.views.drop;

import java.util.List;

import com.gethotdrop.android.R;
import com.gethotdrop.android.data.Comment;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CommentArrayAdapter extends ArrayAdapter<Comment> {
    Context context;
    int layoutResourceId;
    List<Comment> data;

    public CommentArrayAdapter(Context context, int layoutResourceId, List<Comment> commentArray) {
        super(context, layoutResourceId, commentArray);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = commentArray;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CommentHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new CommentHolder();
            holder.comment = (TextView) row.findViewById(R.id.comment_message);
            holder.timestamp = (TextView) row.findViewById(R.id.comment_timestamp);

            row.setTag(holder);
        } else {
            holder = (CommentHolder) row.getTag();
        }

        // map comments to adapter
        // if loading display using adapter
        Comment currentComment = data.get(position);

        holder.comment.setText(currentComment.getMessage());
        if (currentComment.getCreated() != null) {
            holder.timestamp.setText(currentComment.getCreated());
        } else {
            holder.timestamp.setText("");
        }

        //holder.timestamp.setText(thisDrop.getCreatedAt());

        return row;

    }

    static class CommentHolder {
        TextView comment;
        TextView timestamp;
    }
}