package com.gethotdrop.android.views.feed;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.gethotdrop.android.R;
import com.gethotdrop.android.views.drop.DropActivity;

/**
 * Created by Dan on 12/2/13.
 */
public class ListListeners {


    public static class OnClick implements AdapterView.OnItemClickListener {
        private FeedActivity a;

        public OnClick(FeedActivity feed) {
            a = feed;
        }

        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                long arg3) {
            a.imm.hideSoftInputFromWindow(a.list.getWindowToken(), 0);
            //TODO: add grabs to drops
            //postButtons.setBackgroundResource(R.color.grayLight);
            Intent intent = new Intent(a.getBaseContext(), DropActivity.class);
            intent.putExtra("index", arg3);
            a.startActivity(intent);
        }
    }

    public static class OnScroll implements AbsListView.OnScrollListener {
        private FeedActivity a;

        public OnScroll(FeedActivity feed) {
            a = feed;
        }

        // To animate view slide out from bottom to top
        public void slideUp(View view){
            TranslateAnimation animate = new TranslateAnimation(0,0,0,-view.getHeight());
            animate.setDuration(500);
            animate.setAnimationListener( new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                    a.postButtons.bringToFront();
                    a.postButtons.setVisibility(View.GONE);}
                public void onAnimationRepeat(Animation arg0) {}
                public void onAnimationStart(Animation arg0) {}
            });
            a.findViewById(R.id.postNote).bringToFront();
            view.startAnimation(animate);
        }

        // To animate view slide out from top to bottom
        public void slideDown(View view){
            a.findViewById(R.id.postButtons).bringToFront();
            TranslateAnimation animate = new TranslateAnimation(0,0,-view.getHeight(),0);
            animate.setDuration(500);
            animate.setAnimationListener( new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {}
                public void onAnimationRepeat(Animation arg0) {}
                public void onAnimationStart(Animation arg0) {a.postButtons.setVisibility(View.VISIBLE);}
            });
            view.startAnimation(animate);
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            a.imm.hideSoftInputFromWindow(a.list.getWindowToken(), 0);
            //postButtons.setBackgroundResource(R.color.grayLight);
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            if (visibleItemCount > 0) {
                View firstView = view.getChildAt(0);
                if ((firstVisibleItem == 0) && (firstView.getTop() >= 0)) { //&& !atTop) {
                    //	slideDown(postButtons);
                } else if (firstView.getTop() < 0){
                    //	slideUp(postButtons);
                }
            }
        }
    }
}
