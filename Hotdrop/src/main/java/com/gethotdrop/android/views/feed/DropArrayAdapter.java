package com.gethotdrop.android.views.feed;

import java.util.List;

import com.gethotdrop.android.R;
import com.gethotdrop.android.data.Drop;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DropArrayAdapter extends ArrayAdapter<Drop> {
   Context context;
   int layoutResourceId;
   List<Drop> data;
   
   public DropArrayAdapter(Context context, int layoutResourceId, List<Drop> dropArray) {
       super(context, layoutResourceId, dropArray);
       this.layoutResourceId = layoutResourceId;
       this.context = context;
       this.data = dropArray;
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
       View row = convertView;
       DropHolder holder = null;

       if(row == null)
       {
           LayoutInflater inflater = ((Activity)context).getLayoutInflater();
           row = inflater.inflate(layoutResourceId, parent, false);

           holder = new DropHolder();
           holder.note = (TextView)row.findViewById(R.id.note);
           holder.image = (ImageView)row.findViewById(R.id.image);
           holder.grabs = (TextView)row.findViewById(R.id.score);
           holder.timestamp = (TextView)row.findViewById(R.id.timestamp);
           
           row.setTag(holder);
       }
       else
       {
           holder = (DropHolder)row.getTag();
       }

       Drop currentDrop = data.get(position);

       holder.note.setText(currentDrop.getMessage());
       holder.grabs.setText("+" + currentDrop.getGrabs());
       holder.timestamp.setText(currentDrop.getCreatedAt());
       //int outImage=R.drawable.ic_camera;
       holder.image.setImageBitmap(null);
      return row;

   }

   static class DropHolder
   {
       ImageView image;
       TextView note;
       TextView timestamp;
       TextView grabs;
   }
}