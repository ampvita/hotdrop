package com.gethotdrop.android.views.drop;

import java.util.List;

import com.gethotdrop.android.R;
import com.gethotdrop.android.data.Comment;
import com.gethotdrop.android.data.Drop;
import com.gethotdrop.android.data.Storage;
import com.gethotdrop.android.services.Worker;
import com.gethotdrop.android.views.feed.FeedActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.view.MenuItem;

public class DropActivity extends Activity {
	protected static CommentArrayAdapter commentAdapter;
    protected static Context con;
    protected int dropId;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
        long index = 0;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fulldrop);
        con = this;

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    index = extras.getLong("index");
		    Log.e("spot", Long.toString(index));
		}

       ActionBar actionBar = getActionBar();
       actionBar.setDisplayHomeAsUpEnabled(true);

       Drop thisDrop = FeedActivity.drops.get((int) index);
        dropId = thisDrop.getId();
       ((TextView)findViewById(R.id.note)).setText(thisDrop.getMessage());
       ((ImageView)findViewById(R.id.image)).setImageBitmap(null);
       ((TextView)findViewById(R.id.score)).setText("+" + thisDrop.getGrabs());
       ((TextView)findViewById(R.id.timestamp)).setText(thisDrop.getCreatedAt());

       List<Comment> comments = Storage.getInstance().getCommentList(thisDrop.getId());
       commentAdapter = new CommentArrayAdapter(this, R.layout.comment_card, comments);
       updateComments();

       ListView commentList = (ListView) findViewById(R.id.comment_list);
       commentList.setAdapter(commentAdapter); // TODO: ADAPTER SET AT END OF LISTENER

       ImageButton comment = (ImageButton)findViewById(R.id.comment_submit);

       comment.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
            EditText text = (EditText)findViewById(R.id.comment);
            String message = text.getText().toString();
            if (message.length() > 0) {
                Intent postIntent = new Intent(con, Worker.class);
                postIntent.putExtra("action", 3);
                postIntent.putExtra("message", message);
                postIntent.putExtra("dropId", dropId);
                con.startService(postIntent);
                text.setText("");
            }
		}
       });
	}

    @Override
    protected void onPause() {
        super.onPause();
        Storage.clearCurrentDrop();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), FeedActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }

    public static void updateComments() {
        if (commentAdapter != null)
            commentAdapter.notifyDataSetChanged();
    }

}
